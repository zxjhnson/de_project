import os
import base64


class Env:
    def __init__(self):
        # 以環境變數方式輸入SMTP_SERVER
        self.SMTP_SERVER = os.environ["SMTP_SERVER"]

        # 以環境變數方式輸入SMTP_PORT
        self.SMTP_PORT = int(os.environ["SMTP_PORT"])

        # 以環境變數方式輸入SMTP_USERNAME
        self.SMTP_USERNAME = os.environ["SMTP_USERNAME"]

        # 以環境變數方式輸入SMTP_PASSWORD 如使用gmail，需使用google應用程式密碼
        self.SMTP_PASSWORD = os.environ["SMTP_PASSWORD"]

        # 以環境變數方式輸入SENDER_EMAIL
        self.SENDER_EMAIL = os.environ["SENDER_EMAIL"]

        # 以環境變數方式輸入SENDER_EMAIL
        self.RECIPIENT_EMAIL = os.environ["SENDER_EMAIL"]
