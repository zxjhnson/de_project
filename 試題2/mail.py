import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import env_loader


class Mail:
    def __init__(self, subject, message):
        # 設置SMTP服務參數
        self.SMTP_SERVER = env_loader.Env().SMTP_SERVER
        self.SMTP_PORT = env_loader.Env().SMTP_PORT
        self.SMTP_USERNAME = env_loader.Env().SMTP_USERNAME

        # 如使用gmail，需使用google應用程式密碼
        self.SMTP_PASSWORD = env_loader.Env().SMTP_PASSWORD
        self.SENDER_EMAIL = env_loader.Env().SENDER_EMAIL
        self.RECIPIENT_EMAIL = env_loader.Env().RECIPIENT_EMAIL
        self.subject = subject
        self.message = message

        # 建立SMTP連接
        self.smtp_connection = smtplib.SMTP(self.SMTP_SERVER, self.SMTP_PORT)
        self.smtp_connection.starttls()
        self.smtp_connection.login(self.SMTP_USERNAME, self.SMTP_PASSWORD)

    # 寄出通知mail
    def send_email(self):
        msg = MIMEMultipart()
        msg["From"] = self.SENDER_EMAIL
        msg["To"] = self.RECIPIENT_EMAIL
        msg["Subject"] = self.subject
        msg.attach(MIMEText(self.message, "plain"))
        self.smtp_connection.send_message(msg)
