import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from flask import Flask, request, jsonify
from mail import Mail

# 建立app框架
app = Flask(__name__)


# 客製化LogFileHandler中on_modified的處理條件
class LogFileHandler(FileSystemEventHandler):
    def __init__(self, log_file):
        super().__init__()
        self.log_file = log_file
        self.last_line = None

    # 在logs文件修改時觸發
    def on_modified(self, event):
        with open(self.log_file, "r") as f:
            lines = f.readlines()
            if lines:

                # 判斷出logs文件內最後一筆紀錄
                last_line = lines[-1]
                if last_line != self.last_line:
                    self.last_line = last_line

                    # 如果最後一筆紀錄狀態為input_error則寄出通知mail
                    if "input_error" in last_line:
                        Mail("input_error", last_line).send_email()

                    # 如果最後一筆紀錄狀態為over_100則寄出通知mail
                    elif "over_100" in last_line:
                        Mail("over_100", last_line).send_email()


# 讀取日誌文件，如需讀取歷史logs文件則加入參數logsPage=頁數，如果沒有該logs文件則回傳日誌文件不存在
@app.route("/logs", methods=["GET"])
def get_logs():

    logFileName = "/app/logs/app.log"
    logsPage = request.args.get("logsPage")
    if logsPage:
        logFileName = logFileName + "." + logsPage

    try:
        with open(logFileName, "r") as log_file:
            logs = log_file.read()
        return logs, 200
    except FileNotFoundError:
        return "日誌文件{}不存在".format(logFileName), 404


# 啟動app與監視器
if __name__ == "__main__":

    # 監聽的日誌文件路徑
    log_file_path = "/app/logs/app.log"

    # 創建監視器和事件處理器
    observer = Observer()

    # 監視器中帶入客製化後的事件處理器與監視檔案路徑
    event_handler = LogFileHandler(log_file_path)
    observer.schedule(
        event_handler,
        path="/app/logs",
        recursive=False,
    )
    # 啟動監視器
    observer.start()

    # 啟動app
    app.run(host="0.0.0.0", port=5001)

    # 持續運行監視器，除非手動停止
    try:
        while True:
            time.sleep(0.01)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
