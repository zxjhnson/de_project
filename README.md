# 架設流程

## 試題1
### 1. 確認是否安裝docker
```console
foo@bar:~$ docker version
```
### 2. 移動置試題1專案路徑下
```console
foo@bar:~$ cd 試題1
```
應有以下檔案

![image](./pic/試題1資料夾.png)

### 3. 建立image addapp
```console
foo@bar:~$ docker build -t addapp .
```
### 4. 啟用 addapp
```console
foo@bar:~$ docker run --name addapp  -p 5000:5000 -v /logs:/app/logs -d  addapp
```
### 排錯
如果在執行步驟`4. 啟用 addapp`時出現以下訊息
```console
docker: Error response from daemon: Conflict. The container name "/addapp" is already in use by container "<container_id>". You have to remove (or rename) that container to be able to reuse that name.
```
請先執行以下指令
```console
foo@bar:~$ docker rm -f <container_id>
```

再繼續執行
```console
foo@bar:~$ docker run --name addapp  -p 5000:5000 -v /logs:/app/logs -d addapp
```

## 試題2
### 1. 確認是否安裝docker
```console
foo@bar:~$ docker version
```
### 2. 確認addapp是否運行中
```console
foo@bar:~$ docker ps
```
### 3. 移動置試題2專案路徑下
```console
foo@bar:~$ cd ../
foo@bar:~$ cd 試題2
```
應有以下檔案

![image](./pic/試題2資料夾.png)
### 4. 建立image logsapp
```console
foo@bar:~$ docker build -t logsapp .
```

### 5. 啟用 logsapp
```console
foo@bar:~$ docker run -e SMTP_SERVER=<smtp使用server> -e SMTP_PORT=<smtp使用端口> -e SMTP_USERNAME=<信箱帳號> -e SMTP_PASSWORD=<"信箱密碼"> -e SENDER_EMAIL=<寄出信箱> -e SENDER_EMAIL=<收信信箱> --volumes-from addapp -p  5001:5001 -d logsapp
```


# 測試
測試範例使用Postman

## 試題1
## 測試前置
 - 啟動試題1 container

### 測試狀況1 ：正常加總
#### 執行
1. 以`POST`方式向`http://127.0.0.1:5000/add` 並在Body帶入完整合規變數，如`{"a":43,"b":1}`送出requests
#### 結果
- 回傳 sum結果
![image](./pic/試題1_測試結果1.png)
### 測試狀況2 ：缺少其中一變數
#### 執行
1. 以`POST`方式向`http://127.0.0.1:5000/add` 並在Body帶入缺少其中一變數的參數，如`{"a":43}`送出requests
#### 結果
- 回傳 {"input_error": {"a": 43}}
![image](./pic/試題1_測試結果2.png)
### 測試狀況3 ：輸入變數不為整數
1. 以`POST`方式向`http://127.0.0.1:5000/add` 並在Body帶入缺少其中一變數的參數，如`{"a":43,"b":"b"}`送出requests
#### 結果
- 回傳 {"input_error": {"a": 43,"b": "b"}}
![image](./pic/試題1_測試結果3.png)


## 試題2
## 測試前置 
 - 啟動試題1 container
 - 啟動試題2 container
 - 以完成任意試題1測試並送出logs
### 測試狀況1 ：查詢當前logs
#### 執行
1. 以`GET`方式向`http://127.0.0.1:5001/logs` 送出requests
#### 結果
 - 回傳當前logs
![image](./pic/試題2_測試結果1.png)

### 測試狀況2 ：加總大於100通知
#### 執行
1.  以`POST`方式向`http://127.0.0.1:5000/add` 並在Body帶入完整合規變數且`a`與`b`加總大於100，如`{"a":50,"b":51}`送出requests
#### 結果
- 回傳 sum結果
![image](./pic/試題2_測試結果2_1.png)
- 收到mail [over_100]通知
![image](./pic/試題2_測試結果2_2.png)
### 測試狀況3 ：異常通知
1.  以`POST`方式向`http://127.0.0.1:5000/add` 並在Body帶入缺少其中一變數的參數，如`{"a":43}`送出requests
#### 結果
- 回傳 {"input_error": {"a": 43}}
![image](./pic/試題2_測試結果3_1.png)
- 收到mail [input_error]通知
![image](./pic/試題2_測試結果3_2.png)
### 測試狀況4 ：查詢歷史logs
1. 當發現以`GET`方式向`http://127.0.0.1:5001/logs` 送出requests後回傳的結果因超過當前檔案大小限制而被轉入歷史檔時，如下圖
![image](./pic/試題2_測試結果4_1.png) 
### V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  V  
![image](./pic/試題2_測試結果4_2.png)
2. 可以改以`GET`方式向`http://127.0.0.1:5001/logs?logsPage=1` 送出requests
#### 結果
 - 回傳歷史logs
![image](./pic/試題2_測試結果4_3.png)
