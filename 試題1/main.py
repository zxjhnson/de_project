import logging
from flask import Flask, request, jsonify
from concurrent_log_handler import ConcurrentRotatingFileHandler

# 建立app框架
app = Flask(__name__)

# 建立logs紀錄器
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# 設定logs文件，超過限制10000Bytes時轉入歷史資料
crh = ConcurrentRotatingFileHandler("logs/app.log", maxBytes=10000, backupCount=10)

# 設定logs文件格式
crh.setFormatter(
    logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s - %(threadName)s",
        datefmt="%m/%d/%Y %I:%M:%S %p",
    )
)
logger.addHandler(crh)


# 加總功能，確認收到的變數是否符合規定，並回傳加總結果與本筆輸入狀態
@app.route("/add", methods=["POST"])
def add_values():
    data = request.get_json()
    if "a" in data and "b" in data:
        value1 = data["a"]
        value2 = data["b"]

        # 如果輸入資料皆符合規定便進行加總
        if type(value1) == int and type(value2) == int:
            result = value1 + value2

            # 如果加總大於100，本筆輸入狀態為over_100
            if result > 100:
                status = "over_100"

            # 如果加總沒有大於100，本筆輸入狀態為success
            else:
                status = "success"
            logger.info("a=%d, b=%d,sum=%d,status=%s", value1, value2, result, status)
            return jsonify({"sum": result}), 200
        else:

            # 如果輸入資料不符合規定，本筆輸入狀態為input_error
            status = "input_error"
            logger.error("a=%s, b=%s,status=%s", str(value1), str(value2), status)
            return jsonify({"input_error": data}), 200

    # 如果輸入資料不符合規定，本筆輸入狀態為input_error
    else:
        status = "input_error"
        logger.error("input=%s,status=%s", str(data), status)
        return jsonify({"input_error": data}), 200


# 啟動app
if __name__ == "__main__":
    app.run(debug=True)
